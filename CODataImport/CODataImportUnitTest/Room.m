//
//  Room.m
//  CODataImport
//
//  Created by Tran Kien on 1/18/16.
//  Copyright © 2016 Tran Kien. All rights reserved.
//

#import "Room.h"

@implementation Room

// Insert code here to add functionality to your managed object subclass
+ (NSString *)primaryKey {
  return @"room_id";
}

@end
